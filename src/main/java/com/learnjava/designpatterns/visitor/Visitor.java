package com.learnjava.designpatterns.visitor;

/**
 *
 * @author fernando
 */
public interface Visitor {
    
    void visit(JsonElement je);
    
    void visit(XmlElement xe);
}
